//SPDX-License-Identifier: GPL-3.0
pragma solidity ^0.8.2;

import "@openzeppelin/contracts/proxy/utils/Initializable.sol";
import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "@openzeppelin/contracts/token/ERC20/utils/SafeERC20.sol";
import "./IAccount.sol";

/**
 * An account which is deployed for user deposits.
 * Users can send funds into this contract and have their funds "swept" into
 * the Fidira exchange.
 */
contract Account is Initializable, IAccount {
    using SafeERC20 for IERC20;

    address public proxy;

    /**
     * Initializes the account.
     * @dev Follows the OpenZeppelin create2 design pattern.
     * @param proxy_ The withdrawal proxy.
     */
    function initialize(address proxy_) public initializer {
        proxy = proxy_;
    }

    /**
     * Withdraw the the amount `amount` of token `token` from this account to
     * the address `recipient`.
     * @dev Only the proxy may call this function.
     * @param recipient The address of the account to withdraw the tokens to.
     * @param token The address of the ERC20-compatible token.
     * @param amount The amount of tokens to withdraw.
     */
    function withdraw(
        address recipient,
        address token,
        uint256 amount
    ) external {
        require(msg.sender == proxy, "Account/not-proxy");
        IERC20(token).safeTransfer(recipient, amount);
    }
}
