//SPDX-License-Identifier: GPL-3.0
pragma solidity ^0.8.2;

import "@openzeppelin/contracts/access/AccessControlEnumerable.sol";
import "./IAccount.sol";

/**
 * A proxy for withdrawals from a designated account.
 */
contract WithdrawalProxy is AccessControlEnumerable {
    bytes32 public constant WITHDRAWER_ROLE = keccak256("WITHDRAWER_ROLE");

    constructor() {
        _setupRole(DEFAULT_ADMIN_ROLE, _msgSender());
    }

    /**
     * Withdraws `amount` of token `token` from `account`.
     * Only addresses listed in the WITHDRAWER_ROLE may withdraw.
     * @param recipient The address of the receiving account. Allows for
     * different tokens to be deposited into different accounts.
     * @param account The address of the Account.
     * @param token The address of the token to withdraw.
     * @param amount The amount of the token to withdraw.
     */
    function withdrawFromAccount(
        address recipient,
        address account,
        address token,
        uint256 amount
    ) external onlyRole(WITHDRAWER_ROLE) {
        IAccount(account).withdraw(recipient, token, amount);
    }
}
