//SPDX-License-Identifier: GPL-3.0
pragma solidity ^0.8.2;

interface IAccount {
    function withdraw(
        address recipient,
        address token,
        uint256 amount
    ) external;
}
