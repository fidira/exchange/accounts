//SPDX-License-Identifier: GPL-3.0
pragma solidity ^0.8.2;

import "@openzeppelin/contracts/utils/Create2.sol";
import "@openzeppelin/contracts/access/Ownable.sol";
import "./Account.sol";

/**
 * A simple account factory for creating accounts with a predetermined address.
 * Uses the create2 addressing pattern.
 */
contract AccountFactory is Ownable {
    /**
     * Deploys a new account using the `salt` with withdrawal proxy `proxy`.
     * @dev Only the owner of the account factory can call this function.
     * @dev When successful, the AccountCreated event is triggered.
     * @param salt A precalculated salt for ensuring an account is deployed to
     * a predetermined address.
     * @param proxy The address of the withdrawal proxy, a contract which can
     * be used to sweep funds to various accounts, or even through a variety of
     * other smart contracts.
     */
    function deployAccount(bytes32 salt, address proxy)
        external
        onlyOwner
        returns (address accountAddress)
    {
        accountAddress = Create2.deploy(0, salt, type(Account).creationCode);
        Account(accountAddress).initialize(proxy);

        emit AccountCreated(accountAddress);
    }

    /**
     * Computes the address of the Account contract.
     * @dev Use this function when calculating the address of an account.
     * @param salt A precalculated salt for ensuring an account is deployed to
     * a predetermined address.
     */
    function computeAddress(bytes32 salt) external view returns (address) {
        return
            Create2.computeAddress(salt, keccak256(type(Account).creationCode));
    }

    /**
     * Triggered when an account is deployed.
     * @param accountAddress The address of the account contract.
     */
    event AccountCreated(address indexed accountAddress);
}
