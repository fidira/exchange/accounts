import { HardhatUserConfig } from "hardhat/config";
import { HardhatRuntimeEnvironment } from "hardhat/types";
import "@nomicfoundation/hardhat-toolbox";
import "hardhat-deploy";
import "@nomiclabs/hardhat-ethers";
import { mnemonic, privateKey, etherscanAPIKey, reportGas, networkConfig } from "./config";

declare global {
  var hre: HardhatRuntimeEnvironment;
}

const config: HardhatUserConfig = {
  solidity: {
    compilers: [
      {
        version: "0.8.15",
        settings: {
          optimizer: {
            enabled: true,
            runs: 200,
          },
        },
      },
    ],
  },
  namedAccounts: {
    deployer: 0,
    withdrawals: 1,
    user: 2,
  },
  networks: {
    hardhat: {
      accounts: {
        mnemonic: mnemonic,
      },
      forking: {
        url: networkConfig.hardhat.url,
        blockNumber: 19632032,
      },
    },
    localhost: {
      accounts: {
        mnemonic: mnemonic,
      },
      forking: {
        url: networkConfig.hardhat.url,
        blockNumber: 19632032,
      },
    },
    mumbai: {
      url: networkConfig.mumbai.url,
      accounts: {
        mnemonic: mnemonic,
      },
    },
    polygon: {
      url: networkConfig.polygon.url,
      accounts: [
        privateKey
      ],
    },
  },
  gasReporter: {
    enabled: reportGas,
    currency: "USD",
  },
  etherscan: {
    apiKey: etherscanAPIKey,
  },
};

export default config;
