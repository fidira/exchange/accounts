import { ethers, deployments, getNamedAccounts } from "hardhat";
import { Contract } from "ethers";
import { Interface } from "@ethersproject/abi";
import { parseEther } from "@ethersproject/units";
import { expect } from "chai";
import IERC20Artifact from "@openzeppelin/contracts/build/contracts/IERC20.json";
import { SALT } from "./utils/constants";
import { testConfig } from "../config";
import generateAddresses from "./utils/generateAddresses";

// @ts-ignore
import { WithdrawalProxy } from "../typechain-types";

describe("WithdrawalProxy", () => {
  let deployer: string, withdrawals: string;
  let accountAddress: string;
  let withdrawalProxy: WithdrawalProxy;
  let wmatic: Contract;
  let abi: any;

  before(async () => {
    ({ deployer, withdrawals } = await getNamedAccounts());

    abi = IERC20Artifact.abi;

    const snippet = {
      inputs: [],
      name: "deposit",
      outputs: [],
      stateMutability: "payable",
      type: "function",
    };

    abi.push(snippet);
  });

  beforeEach(async () => {
    await deployments.fixture(["all"]);
    const accountFactory = await ethers.getContract("AccountFactory");
    withdrawalProxy = await ethers.getContract<WithdrawalProxy>(
      "WithdrawalProxy"
    );

    await withdrawalProxy.grantRole(
      await withdrawalProxy.WITHDRAWER_ROLE(),
      withdrawals
    );

    const tx = await accountFactory.deployAccount(
      SALT,
      withdrawalProxy.address
    );

    const receipt = await tx.wait();

    accountAddress = receipt.events?.pop()?.args?.accountAddress;

    wmatic = new Contract(
      testConfig.wmatic || "",
      abi,
      await ethers.getSigner(deployer)
    );

    await wmatic.deposit({ value: parseEther("3") });
  });

  it("should withdraw tokens from an account", async () => {
    await wmatic.transfer(accountAddress, parseEther("1"));

    await withdrawalProxy
      .connect(await ethers.getSigner(withdrawals))
      .withdrawFromAccount(
        withdrawals,
        accountAddress,
        wmatic.address,
        parseEther("1")
      );

    expect(await wmatic.balanceOf(withdrawals)).to.be.equal(parseEther("1"));
  });

  it("should not withdraw tokens without withdrawer role", async () => {
    await wmatic.transfer(accountAddress, parseEther("1"));

    await expect(
      withdrawalProxy.withdrawFromAccount(
        withdrawals,
        accountAddress,
        wmatic.address,
        parseEther("1")
      )
    ).to.be.revertedWith(
      "AccessControl: account " +
        deployer.toLowerCase() +
        " is missing role " +
        (await withdrawalProxy.WITHDRAWER_ROLE())
    );
  });

  describe("stress test", async () => {
    const accounts: string[] = [];
    let iface: Interface;

    before(() => {
      accounts.push(...generateAddresses());

      const abiEventTransfer = [
        "event Transfer(address indexed from, address indexed to, uint256 value)",
      ];

      iface = new Interface(abiEventTransfer);
    });

    it("should listen for transfers to accounts", async () => {
      const filter = {
        topics: [
          ethers.utils.id("Transfer(address,address,uint256)"),
          null,
          accounts.map((account) => ethers.utils.hexZeroPad(account, 32)),
        ],
      };

      ethers.provider.on(filter, async (log, _event) => {
        const details = iface.parseLog(log);
        const args = details.args;
        expect(args.value).to.be.equal(parseEther("1"));
      });

      await wmatic.transfer(accounts[560], parseEther("1"));

      await wmatic.transfer(accounts[960], parseEther("2"));

      await new Promise((r) => setTimeout(r, 5000));
    });

    it("should poll for transfers to accounts", async () => {
      const fromBlock = (await ethers.provider.getBlock("latest")).number - 1;
      const filter = {
        fromBlock: fromBlock,
        toBlock: "latest",
        topics: [
          ethers.utils.id("Transfer(address,address,uint256)"),
          null,
          accounts.map((account) => ethers.utils.hexZeroPad(account, 32)),
        ],
      };

      await wmatic.transfer(accounts[560], parseEther("1"));

      await wmatic.transfer(accounts[960], parseEther("2"));

      await new Promise((r) => setTimeout(r, 5000));

      const logs = await ethers.provider.getLogs(filter);

      expect(iface.parseLog(logs[0]).args.value).to.be.equal(parseEther("1"));
    });
  });
});
