// builds a unique identifier for salting the create2 addresss.
// Pair a UUID with a unique contract address maybe AcountFactory for a
// globally unique salt.
export const SALT: string = hre.ethers.utils.solidityKeccak256(
  ["address", "string"],
  [hre.ethers.constants.AddressZero, "40d6dd08-6dc1-400e-b841-0339ac4441e6"]
);
