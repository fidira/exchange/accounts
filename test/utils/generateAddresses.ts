import { mnemonic } from "../../config";

export default () => {
  const root = hre.ethers.utils.HDNode.fromMnemonic(mnemonic);

  const child = root.derivePath("m/44'/60'/0'");

  const accounts: string[] = [];

  for (var i = 0; i < 1000; i++) {
    accounts.push(child.neuter().derivePath("0/" + i.toString()).address);
  }

  return accounts;
};
