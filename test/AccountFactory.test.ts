import { deployments, ethers, getNamedAccounts } from "hardhat";
import { Artifact } from "hardhat/types";
import { expect } from "chai";
import { SALT } from "./utils/constants";

// @ts-ignore
import { AccountFactory, WithdrawalProxy } from "../typechain-types";

describe("AccountFactory", () => {
  let user: string, withdrawals: string;
  let accountFactory: AccountFactory;

  let artifactAccount: Artifact;

  let computedAccountAddress: string;

  before(async () => {
    artifactAccount = await deployments.getArtifact("Account");
    ({ withdrawals, user } = await getNamedAccounts());
  });

  beforeEach(async () => {
    await deployments.fixture(["all"]);
    accountFactory = await ethers.getContract<AccountFactory>("AccountFactory");

    const withdrawalProxy = await ethers.getContract<WithdrawalProxy>(
      "WithdrawalProxy"
    );

    await withdrawalProxy.grantRole(
      await withdrawalProxy.WITHDRAWER_ROLE(),
      withdrawals
    );

    computedAccountAddress = ethers.utils.getCreate2Address(
      accountFactory.address,
      SALT,
      ethers.utils.keccak256(artifactAccount.bytecode)
    );
  });

  it("should compute an address for the account", async () => {
    expect(await accountFactory.computeAddress(SALT)).to.be.equal(
      computedAccountAddress
    );
  });

  it("should deploy a new account", async () => {
    const tx = await accountFactory.deployAccount(SALT, withdrawals);

    const receipt = await tx.wait();

    expect(receipt.events?.pop()?.args?.accountAddress).to.be.equal(
      computedAccountAddress
    );
  });

  it("should only allow an admin to deploy a new account", async () => {
    await expect(
      accountFactory
        .connect(await ethers.getSigner(user))
        .deployAccount(SALT, withdrawals)
    ).to.be.revertedWith("Ownable: caller is not the owner");
  });

  it("should know that an account contract exists", async () => {
    await accountFactory.deployAccount(SALT, withdrawals);

    expect(
      await ethers.provider.getCode(computedAccountAddress)
    ).to.not.be.equal("0x");
  });

  it("should know that an account contract does not exist", async () => {
    expect(await ethers.provider.getCode(computedAccountAddress)).to.be.equal(
      "0x"
    );
  });
});
