# Fidira Exchange Accounts

Fidira provides on-chain accounts for the depositing of ERC20 tokens into the Fidira ecosystem.

These tokens are then pooled for further usage in the ecosystem.

This project provides the mechanisms for on-chain creation of accounts and the 
withdrawal of funds from accounts through a proxy withdrawal.

## Setting up the project

The project uses the Hardhat framework for smart contract development. To run these contracts locally, use Hardhat's suite of tools.

To set up the project locally:

```
git clone https://gitlab.com/fidira/exchange/accounts.git
npm install
```

Before running any Hardhat tasks, you will need to configure some private settings:

```
cp .env.example .env
```

The .env file is ignored by Git so any settings here are local to the developer's system. Follow the instructions in the .env.example for required settings.

Next, compile the contracts:

```
npm run compile
```

You can also run the unit tests:

```
npm run test
```

## Deployment

To deploy the contracts, run:

```
npx hardhat deploy --network network-name
```

where `network-name` is the chain you wish to deploy to.

To deploy information about the contracts to sites such as Etherscan and Sourcify, use the Hardhat targets:

```
npx hardhat etherscan-verify --network network-name
npx hardhat sourcify --network network-name
```

### Ownership and Permissions

Various contract interactions require certain permissions. Some interactions can only be executed by the contract's owner. Other interactions require particular access in the form of a role. For example, calling the mint function may require the calling wallet's address have the MINTER_ROLE role assigned to 
it.

Contracts use the Open Zeppelin Ownership or Access Control design patterns.

#### Changing Ownership

Contracts requiring an ownership change can execute the contract's `transferOwnership` function.

#### Changing Access Control

Contracts with functions that use Access Control will need to assign users to each role in order to execute the function correctly.

The contract will list various roles along with the hash of the role. Use the hash when assigning a role to a user.

Role functions may include `grantRole`, for adding a user to a role, and `revokeRole`, for removing a user from a role. There are also other read functions which can provide information about users and the roles they are assigned to.

## Releases

See the release notes for each version of this software for information including contract addresses.
