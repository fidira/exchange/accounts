import "dotenv/config";

export const mnemonic: string = process.env.MNEMONIC || "";
export const privateKey = process.env.PRIVATE_KEY || "";
export const etherscanAPIKey = process.env.ETHERSCAN_API_KEY;
export const reportGas: boolean = true;

export interface networkConfigItem {
  url: string;
  wmatic?: string | undefined;
}

export interface networkConfigInfo {
  [key: string]: networkConfigItem;
}

export const networkConfig: networkConfigInfo = {
  localhost: {
    url: process.env.POLYGON_URL || "",
    wmatic: "0x0d500B1d8E8eF31E21C99d1Db9A6444d3ADf1270",
  },
  hardhat: {
    url: process.env.POLYGON_URL || "",
    wmatic: "0x0d500B1d8E8eF31E21C99d1Db9A6444d3ADf1270",
  },
  mumbai: {
    url: process.env.MUMBAI_URL || "",
  },
  polygon: {
    url: process.env.POLYGON_URL || "",
  },
};

export interface testConfigInfo {
  wmatic: string | undefined;
}

export const testConfig: testConfigInfo = {
  wmatic: networkConfig.hardhat.wmatic,
};
